# Python windows application example

[![pipeline status](https://gitlab.com/gableroux/python-windows-application-example/badges/master/pipeline.svg)](https://gitlab.com/gableroux/python-windows-application-example/commits/master)

This project is ['How to create an executable (.exe) from a Python script in Windows using pyinstaller' blog post](https://ourcodeworld.com/articles/read/273/how-to-create-an-executable-exe-from-a-python-script-in-windows-using-pyinstaller) running in a CI using [`docker-pyinstaller`](https://github.com/cdrx/docker-pyinstaller) docker image 👍

## Running locally

### Windows

```bash
docker run --rm -v "$(pwd):/src/" cdrx/pyinstaller-windows:python3 "pyinstaller file-creator.py -y"
```

### Linux

```bash
docker run --rm -v "$(pwd):/src/" cdrx/pyinstaller-linux:python3 "pyinstaller file-creator.py -y"
```

## License

[MIT](LICENSE.md) © [Gabriel Le Breton](https://gableroux.com)
